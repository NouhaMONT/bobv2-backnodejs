
const express = require('express');
const app = express();
//const cron = require('cron-validate') ;
//var cron = require('node-cron');
//const apiCallFromRequest = require('./data_collect')
const http = require('http');
const apiRouter = express.Router();

var https = require('follow-redirects').https;
 var myGenericMongoClient = require('./util/mygenericMongodb');
const { runInContext } = require('vm');
 var apiMangodb;
 global.bearerT ='';
 var preferenceList ;
 var preferenceDiff=[];
 
 app.use((req, res, next) => {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
  next();
});  

/**************************************************************************************************************************** */
/*******************************APICompanies */
     //get one doccument
     app.use("/companiesfirstOne", function(req, res, next){  
      myGenericMongoClient.genericFindOne('test4',  function (err,test4) { 
                  res.send(test4);
      })
        });
      
           // get allcompanies
     app.use("/allcompanies", function(req, res, next){  
       myGenericMongoClient.genericFindList('stat',  function (err,stat) { 
                   res.send(stat);
       })
         });
     
     
     
     ////getcompanies by City
     app.use("/companiesCity/:city", function(req, res, next){  
       // var code = req.params.city; 
       var cityword = req.params.city; 
       var query = {'city': {$regex:".*(?i)"+ cityword +".*"}}
       console.log(query);
       myGenericMongoClient.genericFindOneinList('apiData',query,function (err, apiData) {
                  res.send(apiData);
       })
     })
     
     ////get companies by metier
     app.use("/companiesMetier/:rome_code/", function(req, res, next){  
       // var size = parseInt(req.params.size);
       var code = req.params.rome_code; 
       var query = {'rome_code': {$regex:".*(?i)"+ code +".*"} }; 
       console.log(query);
       myGenericMongoClient.genericFindOneinList('stat',query,function (err, stat) {
                  res.send(stat);
       })
     })
     
     app.use("/companiesMetierVille/:matched_rome_label/:city", function(req, res, next){  
       var code = req.params.matched_rome_label; 
       var ville = req.params.city; 
       var query = {'matched_rome_label': {$regex:".*(?i)"+ code +".*"} , 'city': {$regex:".*(?i)"+ ville +".*"} }; 
       console.log(query);
       myGenericMongoClient.genericFindOneinList('apiData',query,function (err, apiData) {
                  res.send(apiData);
       })
     })


/*************************1- get allPreference // recupérer la liste des préferences qui existe dans MySQL*****************  */
/***testttttttttttttt */
//var preference= {};
var urlPref = 'https://bob-backspring-app-staging.herokuapp.com/api/authenticate/prefrences';
https.get(urlPref, (resp) => {
  let data = '';
  resp.on('data', (chunk) => {
    data += chunk;
  });
  resp.on('end', () => { 
    preferenceList = JSON.parse(data) ;
   // console.log(preferenceList)
   
  });
}).on("error", (err) => {
  console.log("Error: " + err.message);
});



/************************ 2- find liste des preferences Mongodb + les comparer avec celles récupéres de mysql et stocker la diff dans une 3eme variable PrefDiff//    */

var pref=[]; 
myGenericMongoClient.genericFindList('preference',  function (err,preference) { 
  
  for (var i=0; i< preference.length; i++){
    Reflect.deleteProperty(preference[i], "_id"); 
  }

  var preferencejson = JSON.stringify(preference) ;
      pref= JSON.parse(preferencejson);
     
    for (var i in preferenceList){
      //console.log(preferenceList[0].ville_metier)
    if(pref.findIndex(x => x.ville_metier === preferenceList[i].ville_metier) == '-1' ){
      preferenceDiff.push(preferenceList[i]); 
    }
    
    }
console.log("prefDiff" + preferenceDiff);
for (var i in preferenceDiff){
console.log(preferenceDiff[i].ville_metier)

}
 })

/******************************************* */
 /** 3- méthode pour automatiser le tocken */
/**********************token start */
 function getToken(callback){
// async function getToken(){
  
var uri = '/connexion/oauth2/access_token?realm=/partenaire&Content-Type=application/x-www-form-urlencoded&grant_type=client_credentials&client_id=PAR_monapplication_fc7cc049c051a6670e6d19eeb3c44861785f4368fb14cddfa77ca889150c1e16&client_secret=f90f199c55d0c94926996074b04d6c6851bd55fc890b78f2c5ea140af3d87ec5&scope=application_PAR_monapplication_fc7cc049c051a6670e6d19eeb3c44861785f4368fb14cddfa77ca889150c1e16 api_labonneboitev1';
var res = encodeURI(uri);
const optionstoken = {
  hostname:'entreprise.pole-emploi.fr',
  path:res,
  method: 'POST',
  headers: {
    "Content-Type": "application/x-www-form-urlencoded",
    
  }
}

  
   global.token=''; 
  var request = https.request(optionstoken, function(response){
    var body = ''; 
    response.on("data", function(chunk){
        body += chunk;
    });
    response.on("end", function(){
        let  t = JSON.parse(body);
         token = t.access_token
         bearerT ='Bearer '+token;
         console.log("testBT"+bearerT);
         callback(bearerT);
          
         
          
        
    });
    
});

 request.end();

 
 
} 

// /****************************************************************token end  */
/**méthode pour vérifier si le token est valide  */
exports.isTokenValid = function(tok) {
  if (tok >= 30){
      return false
  } else if (tok <= 10) {
      return false
  } else{
      return true
  }
}

/******************************************* */
 /** 4- méthode pour récuperer les data 1fois/jour en se basant sur la liste prefDiff(préferences ajoutés par les users chaque jour ) et les stocker dans mongo + inserer liste prefDiff dans liste de preferenceMongo(liste finale)*/

 function getData(callback){

  for (var item in preferenceDiff){  
  const options = {
    hostname:'api.emploi-store.fr',
    path:'/partenaire/labonneboite/v1/company/?distance=50&latitude='+preferenceDiff[item].latitude+'&longitude='+preferenceDiff[item].longitude+'&rome_codes='+preferenceDiff[item].code_rome,
    
    json: true,
    port:443,
    followAllRedirects: true,
    followOriginalHttpMethod: true,
    headers: {
    "Authorization": bearerT,
    "content-type": "application/json",
    "accept": "application/json"
  }} ;

  var apicollect = {};
 
  https.get(options, (resp) => {
    let data = '';
    resp.on('data', (chunk) => {
      data += chunk;
      console.log("test start"+data+ "fin");
      console.log(options);
      //console.log(authtoken)
     console.log("testBearerT"+bearerT);
    });
    resp.on('end', () => {
      //console.log("test start"+data+ "fin");
     // apicollect = JSON.parse(data); 
     // apiMangodb = JSON.parse(data).companies;
      
     //myGenericMongoClient.genericInsertOne('stat', apicollect, function (err) { if (err != null) { console.log("insertionError = " + err); } else { console.log("insertion api done ") } });
     //myGenericMongoClient.genericInsertMany('apiData', apiMangodb, function (err) { if (err != null) { console.log("insertionError = " + err); } else { console.log("insertion apiBob done ") } })
     //myGenericMongoClient.genericInsertMany('preference', preferenceDiff, function (err) { if (err != null) { console.log("insertionError = " + err); } else { console.log("insertion preferences done ") } })
    });
  }).on("error", (err) => {
    console.log("Error: " + err.message);
  });
}
 }

/************************************************************************************************************************************************** */

/******************************************* */
 /** 5- méthode pour récuperer les data 1fois /mois en se basant sur la liste pref(listeMongo=listefinale) et les stocker dans mongo */

 function getDataParMois(callback){
 
    for (var item in pref){  
    const options = {
      hostname:'api.emploi-store.fr',
      path:'/partenaire/labonneboite/v1/company/?distance=50&latitude='+pref[item].latitude+'&longitude='+pref[item].longitude+'&rome_codes='+pref[item].code_rome,
      
      json: true,
      port:443,
      followAllRedirects: true,
      followOriginalHttpMethod: true,
      headers: {
      "Authorization": bearerT,
      "content-type": "application/json",
      "accept": "application/json"
    }} ;
  
    var apicollect = {};
   
    https.get(options, (resp) => {
      let data = '';
      resp.on('data', (chunk) => {
        data += chunk;
        console.log("test start"+data+ "fin");
       // console.log(options);
        //console.log(authtoken)
      // console.log("testBearerT"+bearerT);
      });
      resp.on('end', () => {
        console.log("test start"+data+ "fin");
        apicollect = JSON.parse(data); 
        apiMangodb = JSON.parse(data).companies;
        
       myGenericMongoClient.genericInsertOne('stat', apicollect, function (err) { if (err != null) { console.log("insertionError = " + err); } else { console.log("insertion api done ") } });
       myGenericMongoClient.genericInsertMany('apiData', apiMangodb, function (err) { if (err != null) { console.log("insertionError = " + err); } else { console.log("insertion apiBob done ") } })
      
      });
    }).on("error", (err) => {
      console.log("Error: " + err.message);
    });
  }
   }
  
  /************************************************************************************************************************************************** */



/************************************************************************************************************************************************** */ 
/***6-1- Automatisation Robot qui tourne chaque jour à minuit */

//  cron.schedule('0 0 * * *', function () {
//   console.log('insert api chaque jour à minuit');
//   getInsertAPiJour();
// });

 function getInsertAPiJour(){
  getToken(function(bearerT){
    getData(bearerT, function(result){
      console.log(result);
  
    })
  
   })
 }

/************************************************************************************************************************************************** */ 
/***6-2- Automatisation Robot qui tourne 1 fois par mois (à minuit le premier jour du mois)  */

//  cron.schedule('0 0 1 * *', function () {
//   console.log('insert api 1 fois par mois');
//   getInsertAPiMois();
// });


function getInsertAPiMois(){
  getToken(function(bearerT){
    getDataParMois(bearerT, function(result){
      console.log(result);
  
    })
  
   })
 }



// app.listen(8889,()=>{
//   console.log("server started");
  
//  })

module.exports = app;
 



